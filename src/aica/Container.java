package aica;
import jade.core.Runtime;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;

public class Container {
	public static void main(String[] args) {
		try {
			Runtime runtime= Runtime.instance();										//Création d'une instance de runtime
			ProfileImpl profile= new ProfileImpl(false);								//Implémentation d'aucune propriété dans un profile
			profile.setParameter(ProfileImpl.MAIN_HOST, "localhost");					//Les agents seront déployés de manière locale
			AgentContainer agentContainer= runtime.createAgentContainer(profile);		//Création d'un container (non principal)
			//agentContainer.start();
			AgentController AgentController1= agentContainer.createNewAgent("Orchestrator", "aica.agents.Orchestrator", new Object[]{});			//Ajout de l'orchestrator
			AgentController1.start();																												//Démarrage de l'orchestrator
			AgentController AgentController2= agentContainer.createNewAgent("Sensing", "aica.agents.Sensing", new Object[]{});						//Ajout de Sensing
			AgentController2.start();																												//Démarrage de Sensing
			AgentController AgentController3= agentContainer.createNewAgent("SituationAwareness", "aica.agents.SituationAwareness", new Object[]{});//Ajout de SituationAwareness
			AgentController3.start();																												//Démarrage de SiuationAwareness
			AgentController AgentController4= agentContainer.createNewAgent("ActionPlanning", "aica.agents.ActionPlanning", new Object[]{});		//Ajout de ActionPlanning
			AgentController4.start();																												//Démarrage de ActionPlanning
			AgentController AgentController5= agentContainer.createNewAgent("ActionSelection", "aica.agents.ActionSelection", new Object[]{});		//Ajout de ActionSelection
			AgentController5.start();																												//Démarrage de ActionSelection
			AgentController AgentController6= agentContainer.createNewAgent("ActionExecution", "aica.agents.ActionExecution", new Object[]{});		//Ajout de ActionExecution
			AgentController6.start();																												//Démarrage de ActionExecution																										
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
