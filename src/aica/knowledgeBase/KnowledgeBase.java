package aica.knowledgeBase;
import java.io.*;

public class KnowledgeBase {

	private int threatLevel = 0;
	private String classification = "";
	private String port = "";
	
	String filename = "knowledgeBase.txt";
	
	public KnowledgeBase() {
		//faut tout recup du doc
		load();
	}
	
	public void load() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(
					System.getProperty("user.dir") + "/knowledgeBase.txt"));
			String line = reader.readLine();
			while (line != null) {
				System.out.println(line);
				String[] sub = line.split("=");
				System.out.println(sub[0]);
				System.out.println(sub[1]);
				switch(sub[0]) {
				case "threatLevel":
					this.threatLevel = Integer.parseInt(sub[1]);
					break;
				case "classification":
					this.classification = sub[1];
					break;
				case "port":
					this.port = sub[1];
				default:
					System.out.println("Not working.");
					break;
				}
				// read next line
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void save() {
		
		File file = new File(System.getProperty("user.dir") + "/knowledgeBase.txt");
		file.delete();
		
		try
		{
		FileWriter fw = new FileWriter(this.filename,true);
		fw.write("threatLevel="+this.threatLevel);
		fw.write(System.getProperty( "line.separator" ));
		fw.write("classification="+this.classification);
		fw.write(System.getProperty( "line.separator" ));
		fw.write("port="+this.port);
		fw.write(System.getProperty( "line.separator" ));
		fw.close();
		}
		
		catch(IOException ioe)
		{
			System.err.println("IOException" + ioe.getMessage());
		}
	}

	public int getThreatLevel() {
		return threatLevel;
	}

	public void setThreatLevel(int threatLevel) {
		this.threatLevel = threatLevel;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}
}
