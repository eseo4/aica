package aica.agents;
import aica.agents.noyau.Microgent;
import aica.knowledgeBase.KnowledgeBase;
import aica.message.Message;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;


public class ActionSelection extends Microgent {

    private KnowledgeBase kb; 

    @Override
    protected void setup() {


        ParallelBehaviour parallelBehaviour= new ParallelBehaviour();
        addBehaviour(parallelBehaviour);

        parallelBehaviour.addSubBehaviour(new CyclicBehaviour() {

            @Override
            public void onStart() {
                System.out.println("Creation Agent : "+getAID().getName());
                registerTo();
            }

            @Override
            public void action() {
            	ACLMessage ACL= receive();
				if(ACL!=null) {
					
					Message message= new Message();
					try {
						message = (Message) ACL.getContentObject();
					} catch (UnreadableException e) {
						System.err.println("Recuperation de l'object message impossible");
						e.printStackTrace();
					}
							
					System.out.println("Reception "+getAID().getLocalName()+" de "+message.getSender().getLocalName()+"\n"+message.toString());
				
					if (message.getSender().getLocalName().equals("ActionPlanning")) {
						Message msg= new Message();
						kb = new KnowledgeBase();
						
						switch(message.getInstruction()) {
						case "Lance un honey pot":
							
							switch(kb.getClassification()) {
							case "Classification: Generic Protocol Command Decode":
								msg.setSender(getAID());
								msg.setReceiver("ActionExecution");
								msg.setInstruction(message.getInstruction());
								msg.setParametres(message.getParametres());
								sendMsg(msg);
							break;
							
							case "Classification: Attempted Information Leak":
								msg.setSender(getAID());
								msg.setReceiver("ActionExecution");
								msg.setInstruction(message.getInstruction());
								msg.setParametres("tout les ports ouvert");
								sendMsg(msg);
							break;
						
							default:
							break;
							}
						
						default:
						break;
						}

						sendMsg(msg);
            		}
				}
            }
        });
    }
}