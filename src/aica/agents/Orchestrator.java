package aica.agents;
import aica.agents.noyau.Microgent;
import aica.message.Message;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import java.util.HashMap;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;

//Agent Orchestrator : Communication Manager
public class Orchestrator extends Microgent{
	
	//Fonction qui permet d'écrire une chaine dans un fichier .txt
	//Si append==false, on crée le fichier .txt
	//Si append==false, on ajoute les chaines
	public void ecrire_fichier_txt(String chaine, boolean append) {
			try {
				FileWriter fw;
				if(append==true) {fw = new FileWriter("Orchestrator.txt", true);}
				else {fw = new FileWriter("Orchestrator.txt");}
				fw.write("\r\n"+chaine+"\r\n");
				fw.close();
			} catch (IOException e) {
				System.err.println("Ne peut ecrire dans le fichier txt");
				e.printStackTrace();
			}
	}

	@Override
	public void setup() {
		//Object[] args=getArguments();
		HashMap<String , String> routage= new HashMap<String, String>();
		
		
		addBehaviour(new CyclicBehaviour(){

			@Override
			public void onStart() {
				System.out.println(LocalDateTime.now()+"; Creation Orchestrator : "+getAID().getName());		//Afficher la création de l'orchestrator dans la console
				ecrire_fichier_txt(LocalDateTime.now()+"; Creation Orchestrator : "+getAID().getName(), false);//Enregistrer la création de l'orchestrator dans les logs
			}
			
			
			//Fonction qui vérifie si l'adresse IP d'un sender est stockée dans la table de routage (mesure de sécuritée)
			//Entrée : AID d'un agent		Sortie: Boolean, true si l'IP du sender est dans la table, false sinon
			private boolean authentification(AID sender) { 
				if( sender.getHap().equals(routage.get(sender.getLocalName())) ){return true;}
				else {return false;}
			}
			
			
			@Override
			public void action() {
				ACLMessage ACL= receive();													//Récupération des ACL
				if(ACL!=null) {
					
					Message message= new Message();
					try {
						message = (Message) ACL.getContentObject();							//Récupération du contenu de l'Object Message de ACL
					} catch (UnreadableException e) {
						System.err.println("Recuperation de l'object message impossible");
						e.printStackTrace();
					}
					
					AID sender= message.getSender();										//Récupération du sender
					String receiver= message.getReceiver();									//Récupération du receiver
					String protocolVersion= message.getProtocolVersion();					//Récupération du protocole
					
					System.out.println(LocalDateTime.now()+"; Reception Orchestrator de "+sender.getLocalName()+" pour "+receiver+'\n'+message.toString());
					ecrire_fichier_txt(LocalDateTime.now()+"; Reception Orchestrator de "+sender.getLocalName()+" pour "+receiver+"\r\n"+message.toString(), true);
					
					switch(receiver) 
					{
					case "Orchestrator":
						
						String instruction= message.getInstruction();
						
							switch(instruction) 
							{
							case "Creation Microgent":								//Si le destinataire est l'Orchestrator avec pour instruction "Creation Microgent"
								String parametre= message.getParametres();			//On récupère le paramètre de l'Object Message
								
								if (parametre.equals(getAID().getHap())) {			//Vérification de la position locale de l'agent (sécurité pour éviter l'usurpation d'agents)
									routage.put(sender.getLocalName(), parametre);	//Ajout du Microgent dans une BDD pour enregistrer le routage
								}
							
								System.out.println('\n'+"Table de routage : "+routage+'\n');	//Affichage de la table de routage dans la console
								ecrire_fichier_txt("Table de routage : "+routage, true);		//Enregistrement de la table de routage dans les logs
								break;
						
							default:
								//other action by the Orchestrator
								break;
							}
					break;
					
					default:													//Si destinataire autre que l'Orchestrator (autre agent)
						boolean access = authentification(message.getSender());	//On vérifie si l'adresse IP de l'envoyeur est bien celle enregistrée dans la table de routage (permet d'éviter l'usurpation d'agents)
						
						if(access==true && routage.get(receiver)!=null) {		//Si l'IP de l'envoyeur est vérifiée et que celle du destinataire existe dans le routage
							sendMsg(message);									//On envoit le message (on le tranfère)
							System.out.println('\n'+"Message transfered"+'\n'); //Affichage de la confirmation de l'envoie dans la console
							ecrire_fichier_txt("Message transfered", true);		//Enregistrement de la confirmation de l'envoie dans les logs
						}
					break;
					}
					
				}
				else {
					block();
				}
			}
		});
	}
}