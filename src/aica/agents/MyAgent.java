package aica.agents;
import aica.agents.noyau.Microgent;
import aica.message.Message;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

//Exemple d'un agent ayant 2 comportements parallèle (ParallelBehaviour : Cyclic et onTick)

//comportement Cyclic (Tout le temps) :
//La fonction onStart() s'éxecute seulement une fois lors de la création de l'agent. L'agent va s'enregistrer auprès de l'orchestrator (class Microgent fonction : registerTo())
//La fonction Action() d'éxecute dès que onStart() à fini : L'agent va attendre de recevoir un message et afficher toute les informations de ce dernier

//Comportement onTick (Toutes les X ms)
//L'agent va envoyer des Object message (class Message) à l'aide de la fonction sendMsg (class Microgent fonction : sendMsg())

public class MyAgent extends Microgent{
	
	@Override
	protected void setup() {
		Object[] args=getArguments();
		
		ParallelBehaviour parallelBehaviour= new ParallelBehaviour();
		addBehaviour(parallelBehaviour);
		
		parallelBehaviour.addSubBehaviour(new CyclicBehaviour(){

			@Override
			public void onStart() {
				System.out.println("Creation Agent : "+getAID().getName());
				registerTo();
			}
			
			@Override
			public void action() {
				ACLMessage ACL= receive();
				if(ACL!=null) {
					
					Message message= new Message();
					try {
						message = (Message) ACL.getContentObject();
					} catch (UnreadableException e) {
						System.err.println("Recuperation de l'object message impossible");
						e.printStackTrace();
					}
							
					System.out.println("Reception "+getAID().getLocalName()+" de "+message.getSender().getLocalName()+"\n"+message.toString());
					
				}
				else {
					block();
				}
			}
		});
		
		parallelBehaviour.addSubBehaviour(new TickerBehaviour(this, 100000){
			
			@Override
			public void onTick() {
				Message message= new Message(getAID(), "Agent2", null, "coucou", "bien le bonjour");
				sendMsg(message);
			}
		});
		
	}
}
