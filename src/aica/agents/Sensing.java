package aica.agents;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import aica.agents.noyau.Microgent;
import aica.knowledgeBase.KnowledgeBase;
import aica.message.Message;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;


public class Sensing extends Microgent{
	
	private boolean Done = false;
	
	public void AlertMsg(String Parameter) {
		Message message = new Message(getAID(), "SituationAwareness", null, "Work", "");
		message.setParametres(Parameter);
		
		
		sendMsg(message);
		System.out.println("Envoi "+getAID().getLocalName()+" de "+ message.getSender().getLocalName()+"\n"+ message.toString());
	}
	
	public boolean CheckLog(String path) {
		File file = new File("/var/log/suricata/" + path);
		if (file.exists()) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	protected void setup() {
		//Object[] args=getArguments();
		
		ParallelBehaviour parallelBehaviour= new ParallelBehaviour();
		addBehaviour(parallelBehaviour);
		
		parallelBehaviour.addSubBehaviour(new CyclicBehaviour(){

			@Override
			public void onStart() {
				System.out.println("Creation Agent : "+getAID().getName());
				registerTo();
			}
			
			@Override
			public void action() {
				ACLMessage ACL= receive();
				if(ACL!=null) {
					
					Message message= new Message();
					try {
						message = (Message) ACL.getContentObject();
					} catch (UnreadableException e) {
						System.err.println("Recuperation de l'object message impossible");
						e.printStackTrace();
					}
							
					System.out.println("Reception "+getAID().getLocalName()+" de "+message.getSender().getLocalName()+"\n"+message.toString());
					if(message.getInstruction().equals("Process")) {
						BufferedReader reader;
						try {
							
							reader = new BufferedReader(new FileReader("/var/log/suricata/fast.log"));
							//String line = reader.readLine();
							int i;
							String f = "";
							String classification,port;
							while ((i=reader.read())!=-1) {
								f += (char)i;
							}
							int i_classification = f.indexOf("Classification");
							int i_port = f.indexOf("{") + 1;
							i = i_classification;
							while (f.charAt(i) != ']') {
								i++;
							}
							classification = f.substring(i_classification, i);
							i = i_port;
							while (f.charAt(i) != '}') {
								i++;
							}
							port = f.substring(i_port, i);
							AlertMsg(""+classification+","+port);
							reader.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					
				}
				else {
					block();
				}
			}
		});
		
		parallelBehaviour.addSubBehaviour(new TickerBehaviour(this, 10000){
			@Override
			public void onTick() {
				if(CheckLog("fast.log")) {
					if (!Done) {
						Message message= new Message(getAID(), "Sensing", null, "Process", "");
						sendMsg(message);
						Done = true;
					}
				}
			}
		});
	}
}
