package aica.agents;

import aica.agents.noyau.Microgent;
import aica.knowledgeBase.KnowledgeBase;
import aica.message.Message;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

public class ActionPlanning extends Microgent {
	
	private KnowledgeBase kb;
	
	@Override
	protected void setup() {
		//Object[] args=getArguments();
		
		addBehaviour(new CyclicBehaviour(){

			@Override
			public void onStart() {
				System.out.println("Creation Agent : "+getAID().getName());
				registerTo();
			}
			
			@Override
			public void action() {
				ACLMessage ACL= receive();
				if(ACL!=null) {
					
					Message message= new Message();
					try {
						message = (Message) ACL.getContentObject();
					} catch (UnreadableException e) {
						System.err.println("Recuperation de l'object message impossible");
						e.printStackTrace();
					}
							
					System.out.println("Reception "+getAID().getLocalName()+" de "+message.getSender().getLocalName()+"\n"+message.toString());
					
					if (message.getInstruction().equals("Plan")) {
						Message msg = new Message();
						kb = new KnowledgeBase();
						
						switch(kb.getClassification()) {
						case "Classification: Attempted Information Leak":
							msg= new Message(getAID(), "ActionSelection", null, "Lance un honey pot", "certains ports");
							break;
						case "Classification: Generic Protocol Command Decode":
							msg= new Message(getAID(), "ActionSelection", null, "Lance un honey pot", "tout les ports ouvert");
							break;
						default:
							break;
							
						}
						sendMsg(msg);
					}
				}
				else {
					block();
				}
			}
		});
	}
}
