package aica.agents;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.io.FileWriter;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

import javax.swing.*;

import aica.agents.noyau.Microgent;
import aica.message.Message;

import java.awt.*;
import java.awt.event.*;

//ActionActivation : Activate Plan provided by ActionPlanning (ActionSelection was not created due to time shortage)
public class ActionExecution extends Microgent{
	
		
	@Override
	protected void setup() {
		Object[] args=getArguments();
		JFrame popup = new JFrame("ALERTE POPUP");
		
		
		addBehaviour(new CyclicBehaviour(){

			@Override
			public void onStart() {
				System.out.println("Creation Agent : "+getAID().getName());
				registerTo();
			}
				
					
			protected void alerte() {
				popup.setSize(500,120);					//Création de la fenêtre popup
				JPanel couchetexte = new JPanel();		//Création du txt
				JLabel texte = new JLabel("ALERT! We have been attacked and a honeypot has been deployed!");
				//JButton button = new JButton("ok");	//Création du bouton
				couchetexte.add(texte);
				//couchetexte.add(button);
				popup.add(couchetexte);					
				popup.show();
			}
				
				
			protected void honeypot() {		//Lancement de pentbox
				try {
					//Localisation du pentbox.rb qui est l'exécutable du honeypot
					Process process = Runtime.getRuntime().exec("ruby /home/xibalpa/Desktop/AICA/pentbox-master/pentbox-1.8/pentbox.rb");
					System.out.print("Honeypot déployé");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
				
				
			@Override
			public void action() {
				ACLMessage ACL= receive();
					
				if(ACL!= null) {
						
					Message message= new Message();
					try {
						message = (Message) ACL.getContentObject();
					} catch (UnreadableException e) {
						System.err.println("Recuperation de l'object message impossible");
						e.printStackTrace();
					}
					
					System.out.println("Reception "+ getAID().getLocalName()+" de "+ message.getSender().getLocalName()+"\n"+message.toString());
					System.out.println(message.getSender().getLocalName() + "//" + message.getInstruction());
					
					if (message.getSender().getLocalName().equals("ActionSelection") && message.getInstruction().equals("Lance un honey pot")) {
						alerte();
						honeypot();
					}
					else {
						block();
					}
						
				}
				else {
					block();
				}
			}
		});
	}	
}	
		
