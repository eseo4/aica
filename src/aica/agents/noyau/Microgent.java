package aica.agents.noyau;
import java.io.IOException;
import aica.message.Message;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.wrapper.ControllerException;

public class Microgent extends Agent{
	
	//Fonction qui envoit des objets de type message à l'agent spécifié dans l'Object Message (Message.setSender())
	//Si l'agent qui appelle cette fonction est autre que l'orchestrator, le message est envoyé à l'orchestrator
	//Sinon le message est transféré à l'agent concerné
	//Entree : Object Message
	protected void sendMsg(Message message) {
		String receiver= message.getReceiver();
		ACLMessage ACL= new ACLMessage(ACLMessage.INFORM);
		
		if(!getAID().getLocalName().equals("Orchestrator")) {
			ACL.addReceiver(new AID("Orchestrator", AID.ISLOCALNAME));
		}
		else {
			ACL.addReceiver(new AID(receiver, AID.ISLOCALNAME));
		}
		
		try {
			ACL.setContentObject(message);
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		send(ACL);
	}
	
	
	//Fonction qui envoit un message à l'orchestrator l'informant de la création de l'agent afin de pouvoir l'enregistrer dans une table de routage
	protected void registerTo() {
		Message message= new Message(getAID(),"Orchestrator", null, "Creation Microgent", getAID().getHap());
		sendMsg(message);
	}
	
	
	
	//Fonction s'activant avant la migration de l'agent affichant le nom du contenaire avant la migration 
	@Override
	protected void beforeMove() {
		try {
			System.out.println("Avant migration Microgent : "+this.getAID().getName()+" de : "+this.getContainerController().getContainerName());
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	//Fonction s'activant avant la migration de l'agent affichant le nom du contenaire après la migration 
	@Override
	protected void afterMove() {
		try {
			System.out.println("Apr�s migration Microgent : "+this.getAID().getName()+" vers : "+this.getContainerController().getContainerName());
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	//Fonction s'activant avant la destruction de l'agent
	@Override
	protected void takeDown() {
		System.out.println("Destruction Microgent : "+this.getAID().getName());
	}
}