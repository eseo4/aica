package aica.agents;
import aica.agents.noyau.Microgent;
import aica.knowledgeBase.KnowledgeBase;
import aica.message.Message;
import jade.core.Runtime;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;


public class SituationAwareness extends Microgent {
	private KnowledgeBase kb;
	
	@Override
	protected void setup() {
		//Object[] args=getArguments();
		this.kb = new KnowledgeBase();
		
		addBehaviour(new CyclicBehaviour(){

			@Override
			public void onStart() {
				System.out.println("Creation Agent : "+getAID().getName());
				registerTo();
			}
			
			@Override
			public void action() {
				ACLMessage ACL= receive();
				if(ACL!=null) {
					
					Message message= new Message();
					try {
						message = (Message) ACL.getContentObject();
					} catch (UnreadableException e) {
						System.err.println("Recuperation de l'object message impossible");
						e.printStackTrace();
					}
							
					System.out.println("Reception "+getAID().getLocalName()+" de "+message.getSender().getLocalName()+"\n"+message.toString());
					
					
					String[] parts  = message.getParametres().split(",");
					String part1 = parts[0];
					String part2 = parts[1];
					
					if(part1.equals("Classsification: Attempted Information Leak") && part2.equals("TCP")) {
						kb.setThreatLevel(1);
						kb.setClassification(part1);
						kb.setPort(part2);
						kb.save();
						Message msg= new Message(getAID(), "ActionPlanning", null, "Plan", null);
						sendMsg(msg);
					}
					
					if(part1.equals("Classification: Generic Protocol Command Decode") && part2.equals("TCP")) {
						kb.setThreatLevel(1);
						kb.setClassification(part1);
						kb.setPort(part2);
						kb.save();
						Message msg= new Message(getAID(), "ActionPlanning", null, "Plan", null);
						sendMsg(msg);
					}
					
					else {};
				}
				else {
					block();
				}
			}


			protected void takeDown() {
				kb.save();
			}

		});
	}
	
	@Override
	protected void takeDown() {
		kb.save();
	}
	
}
