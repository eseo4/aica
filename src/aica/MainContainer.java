package aica;
import jade.core.Runtime;
import jade.core.ProfileImpl;
import jade.util.ExtendedProperties;
import jade.util.leap.Properties;
import jade.wrapper.AgentContainer;
import jade.wrapper.ControllerException;

public class MainContainer {

	public static void main(String[] args) {
		 Runtime runtime= Runtime.instance();								//Création d'une instance de runtime
		 Properties properties= new ExtendedProperties();					//Création de l'objet propriété
		 properties.setProperty("gui", "true");								//Ajout de la propriété graphique de Jade (-gui)
		 ProfileImpl profile= new ProfileImpl(properties);					//Implémentation de la propriété dans un profile
		 AgentContainer container= runtime.createMainContainer(profile);	//Création du MainContainer
		 try {
			container.start();												//Démarrage du MainContainer (et donc de l'interface graphique JADE)
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
