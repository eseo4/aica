package aica.message;
import jade.core.AID;
import jade.util.leap.Serializable;

public class Message implements Serializable{
	
	private AID sender;
	private String receiver;
	private String protocolVersion;
	
	private String instruction;  	//Intruction à envoyer à receiver
	private String parametres;		//Paramètre de l'instruction à envoyer
	
	
	//Constructeurs
	public Message(AID sender, String receiver, String protocolVersion, String instruction, String parametres) {
		this.sender=sender;
		this.receiver=receiver;
		this.protocolVersion=protocolVersion;
		this.instruction=instruction;
		this.parametres=parametres;
	}
	
	public Message(AID sender, String receiver, String instruction, String parametres) {
		this(sender, receiver, null, instruction, parametres);
	}
	
	public Message() {
		this(null, null, null, null, null);
	}
	

	//Asesseurs en lecture et écriture
	public void setSender(AID sender) {
		this.sender=sender;
	}
	
	public AID getSender() {
		return this.sender;
	}
	
	public void setReceiver(String receiver) {
		this.receiver=receiver;
	}
	
	public String getReceiver() {
		return this.receiver;
	}
	
	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion=protocolVersion;
	}
	
	public String getProtocolVersion() {
		return this.protocolVersion;
	}
	
	public void setInstruction(String instruction) {
		this.instruction=instruction;
	}
	
	public String getInstruction() {
		return this.instruction;
	}
	
	public void setParametres(String parametres) {
		this.parametres=parametres;
	}
	
	public String getParametres() {
		return this.parametres;
	}

	
	//Aucune information sur cette fonctionnalité
	public String toByteSequence() {
		return null;
	}
	
	//Retourne sous forme de string toutes les informations sur le message
	public String toString(){
		return (String) "Sender : "+sender.getLocalName()+", Receiver : "+receiver+", Protocol Version : "+protocolVersion+", Instruction : "+instruction+", Parametres : "+parametres;
	}
}
